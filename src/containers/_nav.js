export default [
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Safe Rent']
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Kira İşlemleri',
    route: '/safe',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Kira Garanti Kredi Başvurusu',
        to: '/safe/createCredit',
      }
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Müşteri İşlemleri',
    route: '/buttons',
    icon: 'cil-cursor',
    _children: [
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Kiracı İşlemleri',
    route: '/rent',
    icon: 'cil-star',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Ödeme Talimatları',
        to: '/payment/paymentOrders',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Kira Başvuru İşlemleri',
        to: '/rentApplication/rentApplication',
      }
    ],
  },
]

