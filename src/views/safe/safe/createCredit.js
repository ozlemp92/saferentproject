import React from 'react'
import { TheSidebar } from '../../../containers';
import { TheContent } from '../../../containers';
import { TheFooter } from '../../../containers';
import { TheHeader } from '../../../containers';
import {
  CBadge,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CCard,
  CCardBody,
  CCardGroup,
  CCardFooter,
  CCardHeader,
  CCol,
  CRow,
  CToast,
  CToastBody,
  CToastHeader,
  CToaster,
  CForm,
  CInput,
  CInputCheckbox,
  CButton,
  CContainer,
  CFormGroup,
  CLabel
} from '@coreui/react'
import routes from '../../../routes'

class createCredit extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
      };

      this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
     
  }

  handleSubmit(e) {
    debugger;
    this.props.history.push("/creditRequest");
  }
  render() {
    return (
    <div className="c-app c-default-layout">
    <TheSidebar/>
    <div className="c-wrapper">
      <TheHeader/>
    
      <div className="c-body">
      <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="8">
            <CCardGroup>
              <CCard className="p-2">
                <CCardBody>
                  <CForm>
                    <p className="text-muted">Aşağıdaki formu doğru ve eksiksiz doldurun! </p>
                    <CInputGroup className="mb-3">
                      <CInputGroupPrepend>
                   
                      </CInputGroupPrepend>
                      <CLabel htmlFor="exampleDropdownFormPassword1">TC Kimlik Numarası</CLabel>
                      <CInput type="text"placeholder="TC Kimlik Numarası" autoComplete="tc" />
                    </CInputGroup>
                    <CInputGroup className="mb-4">
                      <CInputGroupPrepend>               
                      </CInputGroupPrepend>
                      <CLabel htmlFor="exampleDropdownFormPassword1">Cep Telefonu</CLabel>
                      <CInput type="text" placeholder="Cep Telefonu" 
                      autoComplete="current-password" />
                    </CInputGroup>
                    <CRow>
                      <CCol xs="6">
                        <CButton color="primary" onClick={this.handleSubmit} className="px-4">Başvur</CButton>
                      </CCol>
                      <CCol xs="6" className="text-right">
                        <CButton color="link" className="px-0">Geri Dön</CButton>
                      </CCol>
                    </CRow>
                  </CForm>
                </CCardBody>
              </CCard>
      
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  
        <TheContent/>
  
      <TheFooter/>
 

      </div>
      </div>
</div>

   
  )
    }
}

export default createCredit
