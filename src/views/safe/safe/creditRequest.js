import React from 'react'
import { TheSidebar } from '../../../containers';
import { TheContent } from '../../../containers';
import { TheFooter } from '../../../containers';
import { TheHeader } from '../../../containers';
import {
    CBadge,
    CInputGroup,
    CInputGroupPrepend,
    CInputGroupText,
    CCard,
    CCardBody,
    CCardGroup,
    CDropdown,
    CDropdownItem,
    CDropdownMenu,
    CDropdownToggle,
    CCardFooter,
    CCardHeader,
    CCol,
    CRow,
    CToast,
    CToastBody,
    CToastHeader,
    CToaster,
    CForm,
    CInput,
    CInputCheckbox,
    CButton,
    CContainer,
    CFormGroup,
    CLabel
} from '@coreui/react'
import routes from '../../../routes'

const creditRequest = () => {
    return (
        <div className="c-app c-default-layout">
            <TheSidebar />
            <div className="c-wrapper">
                <TheHeader />

                <div className="c-body">
                    <div className="c-app c-default-layout flex-row align-items-center">
                        <CContainer>
                            <CRow className="justify-content-center">
                                <CCol md="8">
                                    <CCardGroup>
                                        <CCard className="p-2">
                                            <CCardBody>
                                                <CForm>
                                                    <h1>KİMLİK BİLGİLERİ</h1>
                                                    <CInputGroup className="mb-3">
                                                        <CLabel htmlFor="exampleDropdownFormPassword1">ADI</CLabel>
                                                        <CInput type="text" placeholder="Adı" autoComplete="tc" />
                                                    </CInputGroup>
                                                    <CInputGroup className="mb-3">
                                                        <CLabel htmlFor="exampleDropdownFormPassword1">SoyAdı</CLabel>
                                                        <CInput type="text" placeholder="Soyadı" autoComplete="tc" />
                                                    </CInputGroup>
                                                    <CInputGroup className="mb-3">
                                                        <CInputGroupPrepend>
                                                        </CInputGroupPrepend>
                                                        <CLabel htmlFor="exampleDropdownFormPassword1">TC Kimlik Numarası</CLabel>
                                                        <CInput type="text" placeholder="TC Kimlik Numarası"
                                                            autoComplete="current-password" />
                                                    </CInputGroup>
                                                </CForm>
                                            </CCardBody>
                                        </CCard>

                                    </CCardGroup>
                                </CCol>
                            </CRow>
                        </CContainer>
                        <CContainer>
                            <CRow className="justify-content-center">
                                <CCol md="8">
                                    <CCardGroup>
                                        <CCard className="p-2">
                                            <CCardBody>
                                                <CForm>
                                                    <h1>KİŞİSEL BİLGİLER</h1>
                                                    <CInputGroup className="mb-3">
                                                        <CInputGroupPrepend>
                                                        </CInputGroupPrepend>
                                                        <CLabel htmlFor="exampleDropdownFormPassword1">Öğrenim Durumu</CLabel>
                                                        <CDropdown inNav>
                                                            <CDropdownMenu>
                                                                <CDropdownItem>Lise</CDropdownItem>
                                                                <CDropdownItem>Universite</CDropdownItem>
                                                                <CDropdownItem>OrtaÖğretim</CDropdownItem>
                                                                <CDropdownItem>Diğer</CDropdownItem>
                                                            </CDropdownMenu>
                                                        </CDropdown>
                                                    </CInputGroup>
                                                    <CInputGroup className="mb-4">
                                                        <CInputGroupPrepend>
                                                        </CInputGroupPrepend>
                                                        <CLabel htmlFor="exampleDropdownFormPassword1">Meslek</CLabel>
                                                        <CDropdown inNav>
                                                            <CDropdownMenu>
                                                                <CDropdownItem>Öğretmen</CDropdownItem>
                                                                <CDropdownItem>Mühendis</CDropdownItem>
                                                                <CDropdownItem>Diğer</CDropdownItem>
                                                            </CDropdownMenu>
                                                        </CDropdown>
                                                    </CInputGroup>
                                                    <CInputGroup className="mb-4">
                                                        <CInputGroupPrepend>
                                                        </CInputGroupPrepend>
                                                        <CLabel htmlFor="exampleDropdownFormPassword1">Aylık Net Gelir(TL)</CLabel>
                                                        <CInput type="text" placeholder="MeslAylık Net Gelir"
                                                            autoComplete="current-password" />
                                                    </CInputGroup>
                                                    <CRow>
                                                        <CCol xs="6">
                                                            <CButton color="primary" className="px-4">Devam</CButton>
                                                        </CCol>
                                                        <CCol xs="6" className="text-right">
                                                            <CButton color="link" className="px-0">Geri Dön</CButton>
                                                        </CCol>
                                                    </CRow>
                                                </CForm>
                                            </CCardBody>
                                        </CCard>

                                    </CCardGroup>
                                </CCol>
                            </CRow>
                        </CContainer>
                    </div>

                    <TheContent />

                    <TheFooter />


                </div>
            </div>
        </div>


    )
}

export default creditRequest
