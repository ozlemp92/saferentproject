import React from 'react'
import { brandSet } from '@coreui/icons'
import { TheSidebar } from '../../../containers';
import { TheContent } from '../../../containers';
import { TheFooter } from '../../../containers';
import { TheHeader } from '../../../containers';
import {
  CBadge,
  CButton,
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CRow
} from '@coreui/react'
const CoreUIIcons = () => {
  return (
    <div className="c-app c-default-layout">
      <TheSidebar/>
      <div className="c-wrapper">
        <TheHeader/>
      
        <div className="c-body">
        <p className="lead">
                  <CButton color="primary" size="lg">Learn More</CButton>
                </p>
          <TheContent/>
           
        </div>
        <TheFooter/>
        </div>
  </div>
  )
}

export default CoreUIIcons
